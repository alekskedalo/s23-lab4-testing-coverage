package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

class postDaoSetPostTests {
    JdbcTemplate mockJdbc;
    PostDAO mockPost;
    Post post;

    static Integer postIndex = 0;
    static String getPostQuery = "SELECT * FROM \"posts\" WHERE id=? LIMIT 1;";
    static String author1 = "Alice";
    static String author2 = "Bob";
    static String message1 = "Hello";
    static String message2 = "Bye";
    static Timestamp creationTime1 = new Timestamp(0);
    static Timestamp creationTime2 = new Timestamp(1);

    @BeforeEach
    void init() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        mockPost = new PostDAO(mockJdbc);
        post = makePost(author1, message1, creationTime1);
        Mockito.when(mockJdbc.queryForObject(
                Mockito.eq(getPostQuery),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(postIndex)
            )
        ).thenReturn(post);
    }

    static Post makePost(String author, String message, Timestamp creationTime) {
        Post post = new Post();
        post.setAuthor(author);
        post.setMessage(message);
        post.setCreated(creationTime);
        return post;
    }

    static List<Arguments> generateArguments() {
        return List.of(
            Arguments.of(makePost(author1, message1, creationTime2), "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost(author1, message2, creationTime1), "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost(author1, message2, creationTime2), "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost(author2, message1, creationTime1), "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost(author2, message1, creationTime2), "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost(author2, message2, creationTime1), "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost(author2, message2, creationTime2), "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;")
        );
    }

    @ParameterizedTest
    @MethodSource("generateArguments")
    void testSetPostSuccessful(Post newPost, String expected) {
        PostDAO.setPost(postIndex, newPost);
        Mockito.verify(mockJdbc).update(Mockito.eq(expected), Optional.ofNullable(Mockito.anyVararg()));
    }

    @Test
    void testSetPostNoEffect() {
        PostDAO.setPost(postIndex, post);
        Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.anyVararg()));
    }
}